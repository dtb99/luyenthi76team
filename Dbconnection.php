<?php

class DbConnection
{
    protected $username = "1162803";
    protected $password = "Bien20031999";
    protected $host = "localhost";
    protected $database = "1162803";
    protected $tableName;
    protected $queryParams = [];
    protected static $connectionIn = null;

    public function __construct()
    {
        $this->connect();
    }

    public function connect()
    {
        if (self::$connectionIn === null) {
            try {
                self::$connectionIn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->database, $this->username, $this->password);
                self::$connectionIn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (Exception $ex) {
                echo "Error" . $ex->getMessage();
                die();
            }
        }
        return self::$connectionIn;
    }

    public function query($sql, $param = [])
    {
        $q = self::$connectionIn->prepare($sql);
        if (is_array($param) && $param) {
            $q->execute($param);
        } else {
            $q->execute();
        }
        return $q;
    }

    public function buildQueryParams($params)
    {
        $default = [
            "select" => "*",
            "where" => "",
            "other" => "",
            "params" => "",
            "field" => "",
            "value" => []
        ];
        $this->queryParams = array_merge($default, $params);
        return $this;
    }

    public function buildCondition($condition)
    {
        if (trim($condition)) {
            return "where " . $condition;
        }
        return "";
    }

    public function select()
    {
        $sql = "select " . $this->queryParams["select"] . " from " . $this->tableName . ""
            . " " . $this->buildCondition($this->queryParams["where"]) . " " . $this->queryParams["other"];
        $query = $this->query($sql, $this->queryParams["params"]);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function selectOne()
    {
        $this->queryParams["other"] = "limit 1";
        $data = $this->select();
        if ($data) return $data[0];
        return [];
    }

    public function insert()
    {
        $sql = "insert into " . $this->tableName . "  " . $this->queryParams["field"];
        $result = $this->query($sql, $this->queryParams["value"]);
        if ($result) {
            return self::$connectionIn->lastInsertId();
        } else {
            return FALSE;
        }
    }

    public function delete()
    {

    }
}