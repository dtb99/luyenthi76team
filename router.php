<?php

class router
{
    const PARAM_NAME = "r";
    const HOME_PAGE = "home";
    const INDEX_PAGE = "index";
    public static $sourcePath;

    public function __construct($sourcePath = "")
    {
        if ($sourcePath)
            self::$sourcePath = $sourcePath;
    }

    public function getGET($name = NULL)
    {
        if ($name !== NULL) {
            return isset($_GET[$name]) ? $_GET[$name] : NULL;
        }
        return $_GET;
    }

    public function getPOST($name = NULL)
    {
        if ($name !== NULL) {
            return isset($_POST[$name]) ? $_POST[$name] : NULL;
        }
        return $_POST;
    }

    public function router()
    {
        $url = $this->getGET(self::PARAM_NAME);
        if (!$url || $url == self::INDEX_PAGE || !is_string($url)) $url = self::HOME_PAGE;
        $path = self::$sourcePath . "/" . $url . ".php";
        if (file_exists($path)) {
            return require_once $path;
        } else return $this->pageNotFound();
    }

    public function pageNotFound()
    {
        echo "404 page not found";
        die();
    }

}