﻿<!DOCTYPE html>
<?php
include 'UserID.php';
$user = new UserID();
if(!$user->isLogin()) header('Location:login.php');
?>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Thi thử-Anh Văn 1</title>
		<link rel="stylesheet" type="text/css" href="examstyle.css" />
		
		
	</head>
	<body>
		<h1>Đề thi thử Tiếng Anh-Đề 1</h1>
		<p id="time">Thời gian: 60p</p>
		<script src='examform.js'></script>
		<div id="content">
			<div id="noi_dung_de" style='display:none;min-height:3000px'>
				
				Many graffiti _________ without the permission of the owner of the wall.
					@are writing
					@are written
					@~is writing
					@is written`
				You should be very ________ to your teachers for their help.
					@considerate
					@~thankful
					@gracious
					@grateful`
				You can _______ your shorthand ability by taking notes in shorthand during lectures.
					@keep up
					@keep back
					@~keep in
					@keep on`
				Scientists now understand ________
					@how birds navigate over long distances.
					@how to navigate over long distances the birds.
					@~how to distance the birds from navigating.
					@how long distances navigate the birds.`
				"Which hat do you like best?" -"_________________"
					@Yes, I like it best.
					@The one I tried on first.
					@Which one do you like?
					@~No, I haven't tried any.`
				The federal government <u>recommends that</u> all <u>expectant</u> women <u>will not only refrain</u> from smoking but also avoid places where other people <u>smoke</u>. (need correction)
					@expectant
					@~will not only refrain
					@recommends that
					@smoke`
				It took the mayor <u>over an</u> hour <u>explanation</u> to the other members <u>of</u> the board why <u>he had missed</u> the last meeting. (need correction)
					@he had missed
					@~explanation
					@over an
					@of`
				I'm so glad that he spoke <u>in my behalf</u> because I <u>felt awful</u> that I couldn't <u>make it</u> to <u>the event</u>. (need correction)
					@felt awful
					@~in my behalf
					@make it
					@the event`
				Indicate the word that differs from the other three in the position of primary stress.
					@~solution
					@energy
					@quality
					@compliment`
				Indicate the word that differs from the other three in the position of primary stress.
					@secure
					@~angry
					@polite
					@complete`
				Indicate the word whose underline part differs from the other three in pronunciation.
					@fl<u>oo</u>r
					@d<u>oo</u>r
					@~n<u>oo</u>dle
					@b<u>oa</u>rd`
				Indicate the word whose underline part differs from the other three in pronunciation.
					@~visit<u>s</u>
					@destroy<u>s</u>
					@believe<u>s</u>
					@depend<u>s</u>`
				They believe that burning fossil fuels is the main cause of air pollution. (closet meaning)
					@It is believed that air pollution is mainly to blame for burning fossil fuels.
					@~It is believed that burning fossil fuels is held responsible for air pollution.
					@Burning fossil fuels is believed to result from air pollution.
					@Burning fossil fuels is believed to have caused high levels of air pollution.`
				"I haven’t been very open-minded." said the manager.(closet meaning)
					@~The manager admitted not having been very open-minded.
					@The manager denied having been very open-minded.
					@The manager promised to be very open-minded.
					@The manager refused to have been very open-minded.`
				Peter used to work as a journalist for a local newspaper.(closet meaning)
					@~Peter has stopped working as a journalist for a local newspaper.
					@Peter no longer likes the job as a journalist for a local newspaper.
					@Peter refused to work as a journalist for a local newspaper.
					@Peter enjoyed working as a journalist for a local newspaper.`
				My mom is always <i>bad-tempered</i> when I leave my room untidy.(closet meaning to bad-tempered)
					@feeling embarrassed
					@talking too much
					@very happy and satisfied
					@~easily annoyed or irritated`
				I could see the finish line and thought I was <i>home and dry</i>.(closet meaning to home and dry)
					@~successful
					@hopeful
					@hopeless
					@unsuccessful`
				He <i>didn’t bat an eyelid</i> when he realized he failed the exam again.(opposite meaning to didn't bat an eyelid)
					@wasn't happy
					@didn't want to see
					@didn't care
					@~didn't show surprise`
				The US troops are using much more <i>sophisticated</i> weapons in far East.(opposite meaning to sophiticated)
					@expensive
					@complicated
					@~simple and easy to use
					@difficult to operate`
				<i>Read the following passage and mark the letter A, B, C or D to indicate the correct word or phrase that best fits each of the numbered blanks from 20 to 24</i><br>
				Many animals in the wild are suspicious and fearful of human beings. Many animals would take escape instantly (20)......a human approaches. Man, however, soon discovered that some animals can be tamed or domesticated. Unlike animals in the wild, these animals would (21).......man to come close to them. They would even allow their owners to stroke or pet them.<br>
				In the early times man would domesticate animals by setting traps to catch their young. A young animal is (22).........easily domesticated than an adult one. From young,the animal learns to trust and obey its owner.<br>
				Many different kinds of animals have been domesticated. Some common examples are animals like horses, elephants, chickens and pigs. The dog, which is also (23).........as "man's best friend", is one of the first animals to have been domesticated. In England, long ago, the pig was a wild animal. It was a ferocious and aggressive (24).........which was not easily captured. Yet nowadays, the domesticated pig is no longer the lean and tough than creature it used to be.
					@when
					@whether
					@where
					@~while`
				 
					@allow
					@~make
					@tolerate
					@let`
				 
					@quite so
					@lots more
					@far more
					@~so much`
				 
					@~identified
					@known
					@considered
					@recognized`
				 
					@~species
					@individual
					@A & B
					@creature`
				_________ he followed my advice, he __________ be unemployed now.
					@Unless/ can't
					@~Should/ will
					@Were/ would not
					@Had/ would not`
				_________ little boy could remember what he had read from _______ book.
					@A/ the
					@The/ a
					@~The/ the
					@X/ the`
				The judge said that he was ________ by the high standards of performance by the riders.
					@~excited
					@impressed
					@interested
					@imposed`
				Be careful how you _______ that jug. It will break very easily.
					@~pour
					@operate
					@handle
					@employ`
				After driving for five hours, the driver pulled into _______ for a rest.
					@~a round about
					@a bypass
					@a lay-by
					@a flyover`
				The woman __________ when the police told her that her son had died.
					@broke away
					@~broke down
					@broke into
					@broke in`
				Mary: "Thanks a lot for your help". -John: " _______________".
					@My happiness
					@My excitement
					@My delight
					@~My pleasure`
				My friend always dreams of having ____________________.
					@~a small red sleeping bag.
					@red sleeping small a bag.
					@small a bag red sleeping.
					@a bag small red sleeping.`
				David  graddol, a British linguist, believes that English __________ 80% of computer-based communication in the 1990s.
					@made over
					@took up
					@answered for
					@~accounted for`
				<i>Read the following passage and mark the letter A, B, C or D on your answer sheet to indicate the correct answer to each of the questions from 34 to 41</i><br>
				Overpopulation, the situation of having large numbers of people with too few resources and too little space, is closely associated with poverty. It can result from high population density, or  from low amounts of resources, or from both. Excessively high population densities put stress on available resources. Only a certain number of people can be supported on a given area of land, and that number depends on how much food and other resources the land can provide. In countries where people live primarily by means of simple farming, gardening, herding, hunting, and gathering, even large areas of land can support only small numbers of people because these labor - intensive subsistence activities produce only small amounts of food.<br>
				In developed countries such as the United States, Japan and the countries of Western Europe, overpopulation generally is not considered a major cause of poverty. These countries produce large quantities of food through mechanized farming, which depends on commercial fertilizers, large - scale irrigation, and agricultural machinery. This form of production provides enough food to support the high densities of people in metropolitan areas.<br>
				A country's level of poverty can depend greatly on its mix of population density and agricultural productivity. Bangladesh, for example, has one of the world's highest population densities, with 1,147 persons per sq km. A large majority of the people of Bangladesh engage in low - productivity manual farming, which contributes to the country's extremely high level of poverty. Some of the smaller countries in Western Europe, such as the Netherlands and Belgium, have high population densities as well. These countries practice mechanized farming and are involved in high - tech indutries, therefore, they have high standards of living.<br>
				At the other end of the spectrum, many countries in sub - Saharan Africa have population densities of less than 30 persons per sq km. Many people in these countries practice manual subsistence farming, these countries also have infertile land, and lack the economic resources and technology to boost productivity. As a consequence, these nations are very poor. The United States has both relatively low population density and high agricultural productivity, it is one of the world's weathiest nations.<br>
				High birth rates contribute to overpopulation in many developing countries. Children are asset to many poor families because they provide labor, usually for farming. Cultural norms in traditionally rural societies commonly sanction the value of large families. Also, the goverments of developing countries often provide little or no support, financial or political, for farming planning, even people who wish to keep their families small have difficulty doing so. For all those reasons, developing countries tend to have high rates of population growth.<br><br>
				Which of the following is given a definition in paragraph 1?
					@Povertry
					@~Overpopulation
					@Population density
					@Simple farming`
				What will suffer when there are excessively high population densities?
					@~Available resources
					@Skilled labor
					@Land area
					@Farming methods`
				The phrase "that number" in paragraph 1 refers to the number of ______.
					@resources
					@countries
					@densities
					@~people`
				In certain countries, large areas of land can only yield small amounts of food because ________.
					@there are small numbers of laborers
					@~there is lack of mechanization
					@there is no shortage of skilled labor
					@there is an abundance of resources`
				Bangladesh is a country where the level of poverty depends greatly on ____________.
					@its population density only
					@its high agricultural productivity
					@~both population density and agricultural productivity
					@population density in metropolitan areas`
				Which of the following is TRUE, according to the passage?
					@In sub-Saharan African countries, productivity is boosted by technology.
					@All small countries in Western Europe have high population densities.
					@There is no connection between a country's culture and overpopulation.
					@~In certain developed countries, mechanized farming is applied.`
				Which of the following is a contributor to overpopulation in many developing countries?
					@Economic resources
					@~High birth rates
					@Sufficient financial support
					@High-tech facilities`
				Which of the following could be the best title for the passage?
					@High Birth Rate and its Consequences
					@Poverty in Developing Countries
					@~Overpopulation: A Cause of Poverty
					@Overpopulation: A Worldwide Problem`
				<i>Read the following passage and mark the letter A, B, C, or D on your answer sheet to indicate the correct answer to each of the questions from 42 to 48</i><br>
				Today, there are 600 million cars in the world. They may seem like a lot. However, there are over 7 million people on our planet. Most of the world's population uses public transportation to get around. The number of people using public transportation continues to rise. Subway systems worldwide carry 155 million passengers each day. That's more than 30 times the number carried by all the world's airplanes. Every day in Tokyo passengers take more than 40 million rides on public transportation.<br>
				Yet many people see public transportation as "a depressing experience", says author Taras Gresco. They say it is slow, crowded, or too expensive. In fact, Gresco says, <u>it</u> is actually "faster, more comfortable and cheaper" than driving a car. Like millions of people, Taras Gresco is a "straphanger" – a person who rides public transportation. In his book <i>straphanger: Saving Our Cities and Ourselves from the Automobile</i>, Gresco describe the benefits of public transportation. Firstly, it is better for the environment. When people use public transportation, they use less fuel. Twenty people on one bus use much less fuel than 20 people in 20 cars. Fewer cars mean less pollution and cleaner air.<br>
				Using public transportation can be good for your health in other ways. It can even help you lose weight. In one study, a group of people took public transportation every day for six months. Each day they walked to a bus stop or train station. In six months, each person lost an average of six pounds – almost three kilograms.Taking public transportation has another benefit, says Gresco. It helps people become part of their community. When you are alone in your car, you don't talk to anyone. One Tokyo straphanger told Gresco, "To use public transport is to know how to cooperate with other people", It teaches you "how to behave in a public space". So, public transportation is more than a way to get to work or school. It can help lead to cleaner cities. It may also lead to a healthier and more cooperative world population.<br><br>
				According to the passage, the number of people travelling by planes each day is about......
					@185 million
					@125 million
					@~5 million
					@20 million`
				The word "<u>it</u>" in the passage refers to .........
					@driving a car
					@~public transportation
					@author Taras Gresco
					@depressing experiece`
				What is not true about Taras Gresco according to the reading passage?
					@Taras Gresco finds public transportation beneficial for both the cities and the users.
					@Taras Gresco often travels by public transportation, especially the bus.
					@Taras Gresco wrote a book about the benefits of public transportation.
					@~Taras Gresco launched a campaing to encourage people to use the public transportation.`
				Which of the following is mentioned as a benefit for a public transportation rider?
					@He or she will be able to learn how to ride a means of public transportation.
					@He or she will have a good chance to enjoy the natural landscape.
					@He or she will have a good chance to make more friends with the different people.
					@~He or she will know how to behave in public places and cooperate with others.`
				Which of the following is not mentioned in the passage as the one that benefits from the public transportation?
					@the bus rider
					@the environment
					@~the car driver
					@the city`
				How does the environment benefit from people's use of the public transportation?
					@Subways use renewable energy sources instead of the fossil fuels, making the environment cleaner.
					@The smaller number of cars sold helps the automobile industry save the environment substantially.
					@Buses and trains not only conserve energy but also save public transportation.
					@~Fewer people using private transportation means less fuel used, hence cleaner environment.`
				Which of the following can be the best title of the reading passage?
					@~Public Transportation - We all Benefit
					@Public transportation – to Save the Environment
					@Public Transportation – a way to Loose Weight
					@Public Transportation – Cleaner - Cities`
				They finish one project. They started working on the next. (choose the best combines)
					@Only if they had finished one project did they start working on the next.
					@Had they finished one project, they would have started working on the next.
					@~Hardly had they finished one project when they started working on the next.
					@Not until did they start working on the next project then they finished one.`
				The proposal seemed like a good idea. The manager refused it. (choose the best combines)
					@The proposal didn't seemed like a good idea, so the manager didn't accept it.
					@The manager didn't like the proposal because it didn't seem a good idea.
					@~The manager refused the proposal though it seemed like a good idea.
					@Since the proposal seemed like a good idea, the manager refused it.
			</div>
			<div id="countDown">	
				<p>Thời gian còn lại:</p>
				<p class="minute">Phút</p>
				<p class="second">Giây<p>
				
			<div>
			<script type="text/javascript">
				function getCookie(name){
					var cname = name + "=";
					var dc = document.cookie;
					if (dc.length > 0) {
						begin = dc.indexOf(cname);
						if (begin != -1) {
							begin += cname.length;
							end = dc.indexOf(";", begin);
						if (end == -1) end = dc.length;
						return unescape(dc.substring(begin, end));
						}
					}
					return null;
				}
				if(document.cookie && document.cookie.match('myClock_AV1')){
				  // get deadline value from cookie
				  var deadline = getCookie('myClock_AV1');
				}
				 
				
				else{
				  // create deadline 60 minutes from now
					var currentTime = Date.parse(new Date());
					var deadline = Date.parse(new Date(currentTime + 60*60*1000));
				 
				  // store deadline in cookie for future reference
				  document.cookie = 'myClock_AV1=' + deadline + '; path=/';
				}
							
			
			
				
				var currentTime = Date.parse(new Date());
				
				var t=deadline-currentTime;
				var second = Math.floor((t / 1000) % 60);
				var minute = Math.floor(t / 1000 / 60);
				
				
				
			
				var nd = $("#noi_dung_de").html();
				var cau_hoi = nd.split('`');
				var html = ""; var tra_loi_dung = 0;
				for(var i = 0;i<cau_hoi.length;i++)
				{
					var chi_tiet = cau_hoi[i].split('@');
					//alert(chi_tiet[i]);
					for(var j=0;j<chi_tiet.length;j++)
					{
			 
						if (j==0) html += "<tr><td><b>Câu " + (i+1) + ":</b></td><td> <b>" + chi_tiet[j].trim() + "</b></td></tr>";
						else 
						{
							html += "<tr><td></td><td id='" + (i + 1 + String.fromCharCode(64 + j))+"'><input type='radio' name='" + (i + 1 )+ "' id='" + (i + 1 + String.fromCharCode(64 + j))+"' value='" + chi_tiet[j].trim() +"'> " + String.fromCharCode(64 + j) + ". " + chi_tiet[j].replace("~","").trim() + "</td></tr>";
						}
					}
				}

				$("#noi_dung_de").empty().append("<table>" + html + "</table><input type='submit' value='Nộp bài' id='tra_loi_xong' style='margin-left:585px;'>").fadeIn();
				$("#noi_dung_de input").click(function(){   
				//Lấy id của radio
				var id = $(this).attr("id");
				for (var j=1;j<5;j++){
					var newid=id.substr(0,1)+String.fromCharCode(64 + j);
					
					$("td#" + newid).css("background-color","");
				}
				$("td#" + id).css("background-color","yellow");
				
				});   
				
2				
				
3				
4				
			
				
				/*jQuery(document).ready(function($) {
					
					setTimeout(function(){
						
						$('#tra_loi_xong').trigger( "click" ) ;
						}, time);
				}); */
				
				
				
			
			
				$("#tra_loi_xong").click(function(){
					$('#noi_dung_de input').each(function () {
					var id = $(this).attr("id");
					var ctl = $(this).val();
				 
					//Hiển thị câu đúng với nền là màu đỏ
					if (ctl[0] == '~') 
					{
						//alert($("td#" + id).css("background-color"));
						if ($("td#" + id).css("background-color") == "rgb(255, 255, 0)")
						{
							tra_loi_dung++;
						}
						else $("td#" + id).css("background-color","red");     
					}
				});
				$("#noi_dung_de").append("<p style='text-align:center'>Số câu đúng :" + (tra_loi_dung < 0 ? "0" : tra_loi_dung) + " câu. Điểm : " + tra_loi_dung*0.2+ "</p>");
				$("#tra_loi_xong").fadeOut();
				
				});
				
				var timer = setInterval(function() {
					
				   $('.minute').text(minute +" Phút")
				   $('.second').text(second-- +" Giây");
				   if (second == -1) {
						minute--;
						second=60;
				   }
				   if (minute ==-1)  {
						 alert("Đã hết giờ làm bài, kiểm tra kết quả");
					  $('#tra_loi_xong').trigger( "click" ) ;
					  $('.second').fadeOut("slow");
					  $('.minute').fadeOut("slow");
					  clearInterval(timer);
				   }
				   
				}, 1000);
				
			</script>
			
		</div>
	</body>
</html>